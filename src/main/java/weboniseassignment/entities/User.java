package weboniseassignment.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Size(min = 3, message = "First name must be of at least 3 chars")
    @ApiModelProperty(notes = "First name must of at least 3 chars long")
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 3, message = "Last name must be of at least 3 chars")
    @ApiModelProperty(notes = "Last name must of at least 3 chars long")
    @Column(name = "last_name")
    private String lastName;

    @NotEmpty(message = "Email can not be empty")
    @Email(message = "Please provide valid email")
    @ApiModelProperty(notes = "Email should not be empty")
    @Column(name = "email", unique = true)
    private String email;

    @Size(min = 3, message = "Password must be of at least 3 chars")
    @ApiModelProperty(notes = "Password must be of at least 3 chars")
    @Column(name = "password")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
