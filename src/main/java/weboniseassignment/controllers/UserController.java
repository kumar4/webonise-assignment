package weboniseassignment.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import weboniseassignment.constants.ApiURLs;
import weboniseassignment.entities.User;
import weboniseassignment.exceptions.UserNotFoundException;
import weboniseassignment.services.UserService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(ApiURLs.UserURLs.BASE_URL)
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(ApiURLs.UserURLs.PROFILE)
    public User userProfile(@PathVariable long id) {
        Optional<User> userOptional = userService.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("User with id " + id + " does not exist");
        }

        User user = userOptional.get();
        user.setPassword(null);
        return user;
    }

    @GetMapping(ApiURLs.UserURLs.ALL_USERS)
    public List<User> allUsers() {
        return userService.findAll();
    }
}
