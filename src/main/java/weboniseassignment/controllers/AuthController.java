package weboniseassignment.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import weboniseassignment.constants.ApiURLs;
import weboniseassignment.entities.User;
import weboniseassignment.models.AuthRequest;
import weboniseassignment.models.AuthResponse;
import weboniseassignment.services.AuthService;
import weboniseassignment.services.UserService;
import weboniseassignment.utilities.HateoasUtil;

import javax.validation.Valid;

import static weboniseassignment.constants.ApiURLs.AuthURLs.BASE_URL;

@RestController
@RequestMapping(BASE_URL)
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @PostMapping(ApiURLs.AuthURLs.SIGN_UP)
    public EntityModel<AuthResponse> create(@Valid @RequestBody User user) {
        User savedUser = userService.create(user);
        String token = authService.issueToken(savedUser.getEmail());
        return HateoasUtil.getCreatedUserHateoas(new AuthResponse(token), savedUser.getId());
    }

    @PostMapping(ApiURLs.AuthURLs.LOGIN)
    public AuthResponse login(@Valid @RequestBody AuthRequest req) {
        String token = authService.authenticate(req.getEmail(), req.getPassword());
        return new AuthResponse(token);
    }
}
