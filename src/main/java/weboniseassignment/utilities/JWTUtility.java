package weboniseassignment.utilities;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JWTUtility {

    @Value("${secret}")
    private String secret;
    private static final String CLAIM_USERNAME = "username";

    public String issueToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_USERNAME, userDetails.getUsername());
        return newToken(claims, userDetails.getUsername());
    }

    private String newToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public boolean isValidToken(String token, UserDetails userDetails) {
        String sub = userDetails.getUsername();
        Claims claims = extractClaims(token);
        return claims.getSubject().equals(sub) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        Claims claims = extractClaims(token);
        Date expiration = claims.getExpiration();
        return expiration.before(new Date());
    }

    private Claims extractClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    public String extractUserName(String token) {
        Claims claims = extractClaims(token);
        return claims.getSubject();
    }
}
