package weboniseassignment.utilities;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import weboniseassignment.controllers.UserController;
import weboniseassignment.models.AuthResponse;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class HateoasUtil {
    private HateoasUtil() { }

    public static EntityModel<AuthResponse> getCreatedUserHateoas(AuthResponse response, long id) {
        EntityModel<AuthResponse> model = EntityModel.of(response);
        Link link = linkTo(methodOn(UserController.class).userProfile(id)).withRel("new-user");
        model.add(link);
        return model;
    }
}
