package weboniseassignment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import weboniseassignment.entities.User;
import weboniseassignment.repositories.UserRepository;

import java.text.MessageFormat;
import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
public class JDBCUserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByEmail(email);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), emptyList());
        }

        throw new UsernameNotFoundException(MessageFormat.format("User with email {0} can not be found", email));
    }
}
