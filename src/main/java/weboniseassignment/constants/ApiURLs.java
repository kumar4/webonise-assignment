package weboniseassignment.constants;

public interface ApiURLs {

    String[] AUTH_WHITELIST_URLS = {
            AuthURLs.BASE_URL + ApiURLs.AuthURLs.SIGN_UP,
            AuthURLs.BASE_URL + ApiURLs.AuthURLs.LOGIN,
            H2URLs.BASE_URL,
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/v3/api-docs/**",
            "/swagger-ui/**"
    };

    interface AuthURLs {
        String BASE_URL = "/auth";
        String SIGN_UP = "/sign-up/";
        String LOGIN = "/login/";
    }

    interface UserURLs {
        String BASE_URL = "/user";
        String PROFILE = "/{id}/profile/";
        String ALL_USERS = "/all/";
    }

    interface H2URLs {
        String BASE_URL = "/h2-console/**";
    }
}
