package weboniseassignment.models;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AuthRequest {

    @NotEmpty(message = "Email can not be empty")
    @ApiModelProperty(notes = "Email should not be empty")
    @Email
    private String email;

    @Size(min = 3, message = "Password must be of at least 3 chars")
    @ApiModelProperty(notes = "Password must be of at least 3 chars")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
