## Webonise Assignment
This assignment exposes REST APIs for JWT Based Authentication using Spring Boot

## Servers and Ports
The application uses Apache Tomcat and runs on port 8080

## Database Details
| Property Name | Property Value |
| ------ | ------ |
| DB Dialect | H2 In-Memory DB |
| DB Console URL | http://localhost:8080/h2-console/ |
| DB Url | jdbc:h2:mem:testdb |
| DB Username | sa |
| DB Password | password |

## Controllers
- Auth Controller - This is responsible for sign-up and login functionality.
- User Controller - This is responsible for providing user related info. 

## Auth Controller
Auth Controller currently exposes following APIs, please refer swagger documentation for more details.


| API Endpoint | HTTP METHOD | Description |
| ------ | ------ | ------ |
| /auth/sign-up/ | POST | API for user to sign up |
| /auth/login/ | POST | API for user to login |

## User Controller
User Controller currently exposes following APIs, please refer swagger documentation for more details.


| API Endpoint | HTTP METHOD | Description |
| ------ | ------ | ------ |
| /user/id/profile/ | GET | API to get specific users profile by providing path variable id |
| /user/all/ | GET | API to get all the users in the system |


## Swagger Details
Swagger URL: http://localhost:8080/swagger-ui/